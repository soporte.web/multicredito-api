<?php

use App\Store;
use App\Employee;
use Illuminate\Database\Seeder;

class StoreTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tiendas = [
            [
                'nit' => '1090473636-1',
                'name' => 'Bicicletas Gruiz BGA',
                'address' => 'calle 15 # 27-15',
                'neighborhood' => 'Centro',
                'city_id' => 2,
                'phone' => '3013315544',
                'email' => 'store@gruiz.com',
                'user_id' => '1'
            ],
            [
                'nit' => '1090473636-2',
                'name' => 'Bicicletas Gruiz CUC',
                'address' => 'calle 16 # 13-46',
                'neighborhood' => 'La Libertad',
                'city_id' => 4,
                'phone' => '3013314488',
                'email' => 'store@gruiz.com',
                'user_id' => '1'
            ]

        ];

        $empleados = [
            [
                'employee_id' => '123456789',
                'first_name' => 'Empleado 1',
                'last_name' => 'Contreras',
                'password' => Hash::make('123123'),
                'store_id' => 1
            ],
            [
                'employee_id' => '987654321',
                'first_name' => 'Empleado 2',
                'last_name' => 'Ruiz',
                'password' => Hash::make('123123'),
                'store_id' => 2
            ],
        ];

        foreach ($tiendas as $tienda) {
            Store::create($tienda);
        }

        foreach ($empleados as $emp ) {
            $employee = Employee::create($emp);
            $employee->assignRole('store-employee');
        }
    }
}
