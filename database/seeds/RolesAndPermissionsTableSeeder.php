<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesAndPermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $emp_permissions = [
            ['name' => 'credit-request-create', 'description' => 'Crear Solicitudes de Credito', 'guard_name'=>'emp'],
            ['name' => 'credit-request-edit', 'description' => 'Modificar Solicitudes de credito', 'guard_name'=>'emp'],
            ['name' => 'credit-request-get', 'description' => 'Obtener una Solicitud de Credito especifica', 'guard_name'=>'emp'],
            ['name' => 'customer-create', 'description' => 'Crear el Cliente Final', 'guard_name'=>'emp'],
            ['name' => 'customer-edit', 'description' => 'Modificar el Cliente Final', 'guard_name'=>'emp'],
            ['name' => 'customer-get', 'description' => 'Listar Cliente Especifico', 'guard_name'=>'emp'],
        ];

        $permissions = [
            /**
             * PERMISOS DE USUARIOS
             * TODOS LOS PERMISOS QUE TIENEN LAS TIENDAS TAMBIEN SON DE LOS EMPLEADOS
             * El usuario ADMIN tendrá todos los permisos
             */
            /** Todos los usuarios podran crear solicitudes de credito*/
            ['name' => 'credit-request-create', 'description' => 'Crear Solicitudes de Credito'],
            /** Los usuarios (Tiendas - Empleados) podrán actualizar Solicitudes que requieran modificacion*/
            ['name' => 'credit-request-edit', 'description' => 'Modificar Solicitudes de credito'],
            /** Solamente los Usuarios (Empleados) podrán Aprobar o rechazar las solicitudes de credito */
            ['name' => 'credit-request-approve', 'description' => 'Aprobar, Rechazar o enviar a Modificar estados Solicitudes de credito'],
            /** Solamente los Usuarios (Administradores) podrán obtener todas las solicitudes de Credito*/
            ['name' => 'credit-request-get-all', 'description' => 'Obtener todas las Solicitudes de Credito'],
            /** Solamente los Usuarios (Empleados) podrán obtener todas las solicitudes de Credito Pendientes*/
            ['name' => 'credit-request-get-pendenting', 'description' => 'Obtener todas las Solicitudes de Credito Pendientes'],
            /** Los usuarios (Tiendas - Empleados) podrán consultar una solicitud especifica (Por estado, por ID, etc...)*/
            ['name' => 'credit-request-get', 'description' => 'Obtener una Solicitud de Credito especifica'],

            /**
             * PERMISOS EMPLEADO DE TIENDA
             */
            /**Los usuarios (Store-admin) podrán crear empleados para su tienda */
            ['name' => 'employee-store-create', 'description' => 'Crear Empleado de Tienda'],
            /**Los Usuarios (store-admin) podrán modificar empleados para su teinda  */
            ['name' => 'employee-store-edit', 'description' => 'Modificar Empleado de Tienda'],
            /**Los Usuarios (Store-admin) podrán eliminar empleados para su tienda */
            ['name' => 'employee-store-delete', 'description' => 'Eliminar Empleado de Tienda'],
            /**Solo administradores de tienda */
            ['name' => 'employee-store-transfer', 'description' => 'Transferir Empleado entre Tiendas'],

            /**
             * PERMISOS PARA ADMINISTRAR TIPOS DE CREDITO
             * Solo Administradores
             */
            ['name' => 'credit-type-create', 'description' => 'Crear Tipos de Credito'],
            ['name' => 'credit-type-edit', 'description' => 'Modificar Tipos de Credito'],
            ['name' => 'credit-type-delete', 'description' => 'Eliminar Tipos de Credito'],

            /**
             * PERMISOS PARA ESTADOS DE SOLICITUD
             * Solo administradores
             */
            ['name' => 'status-create', 'description' => 'Crear Estados de Credito'],
            ['name' => 'status-edit', 'description' => 'Modificar Estados de Credito'],
            ['name' => 'status-delete', 'description' => 'Eliminar Estados de Credito'],

            /**
             * PERMISOS PARA LA ADMINISTRACION DE CLIENTES
             */
            /** Los usuarios (Tiendas) Podrán crear Clientes*/
            ['name' => 'customer-create', 'description' => 'Crear el Cliente Final'],
            /** Los usuarios (Tiendas - Empleados) Podrán modificar los clientes */
            ['name' => 'customer-edit', 'description' => 'Modificar el Cliente Final'],
            /** Solamente los usuarios (Empleados) podran eliminar clientes. */
            ['name' => 'customer-delete', 'description' => 'Eliminar el Cliente Final'],
            /** Solamente los Usuarios (Tiendas-Empleados) pordán consultar un cliente especifico */
            ['name' => 'customer-get', 'description' => 'Listar Cliente Especifico'],
            /** Solamente los Usuarios (Empleados) pordán consultar todos los clientes*/
            ['name' => 'customer-get-all', 'description' => 'Listar Todos los Clientes'],

            /**
             * PERMISOS PARA LA ADMINISTRACION DE USUARIOS
             */
            /** Solamente los usuarios (Administrador) podrán eliminar usuarios de la base de datos*/
            ['name' => 'user-delete', 'description' => 'Eliminar el Usuario en la base de datos'],
            /** Solamente los usuarios (Empleados) podrán modificar usuarios de la bsae de datos*/
            ['name' => 'user-edit', 'description' => 'Modificar el Usuario en la base de datos'],

            /**
             * PERMISOS PARA LA ADMINISTRACION DE TIENDAS
             */
            ['name' => 'store-get', 'description' => 'Listar una tienda especifica'],
            ['name' => 'store-get-all', 'description' => 'Listar todas las tiendas'],
            ['name' => 'store-get-my', 'description' => 'Listar Mis Tiendas'],
            ['name' => 'store-edit', 'description' => 'Modificar una tienda'],
            ['name' => 'store-delete', 'description' => 'Eliminar una tienda'],

        ];

        foreach ($permissions as $permission) {
            Permission::create($permission);
        }
        foreach ($emp_permissions as $permission) {
            Permission::create($permission);
        }

        /**
         * ROLES DE USUARIO
         */
        $roles = [
            ['name' => 'store-admin', 'description' => 'Administrador de Tienda'],
            ['name' => 'employee', 'description' => 'Empleado'],
            ['name' => 'admin', 'description' => 'Administrador'],
            ['name' => 'store-employee', 'description' => 'Empleado de Tienda', 'guard_name' => 'emp']
        ];

        foreach ($roles as $role) {
            Role::create($role);
        }

        /**
         * ASIGNANDO PERMISOS AL USUARIO TIENDA
         */
        $store = Role::findByName('store-admin');
        $store_permissions = [
            //
            'credit-request-create', 'credit-request-edit', 'credit-request-get',
            'customer-create', 'customer-edit', 'customer-get',
            'employee-store-edit', 'employee-store-create', 'employee-store-delete', 'employee-store-transfer',
            'store-get-my', 'store-edit', 'store-delete', 'store-get'
        ];
        $store->givePermissionTo($store_permissions);

        /**
         * ASIGNANDO PERSMISOS AL EMPLEADO TIENDA
         */
        $store_employee = Role::where('name', 'store-employee')->first();
        $store_permissions = [
            'credit-request-create', 'credit-request-edit', 'credit-request-get',
            'customer-create', 'customer-edit', 'customer-get'
        ];
        $store_employee->givePermissionTo($store_permissions);

        /**
         * ASIGNANDO PERMISOS AL USUARIO EMPLEADO
         */
        $employee = Role::findByName('employee');
        $employee_permissions = [
            'credit-request-approve', 'credit-request-get-pendenting', 'customer-get-all'
        ];
        $employee->givePermissionTo(array_merge($employee_permissions, $store_permissions));

        /**
         * ASIGNANDO PERMISOS AL USUARIO ADMIN
         */
        $admin = Role::findByName('admin');
        $admin_permissions = [];

        foreach ($permissions as $permission) {
            array_push($admin_permissions, $permission['name']);
        }

        $admin->givePermissionTo($admin_permissions);
    }
}
