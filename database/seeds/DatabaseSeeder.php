<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(RolesAndPermissionsTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(CitiesTableSeeder::class);
        $this->call(StoreTableSeeder::class);
    }
}
