<?php

use App\Employee;
use App\User;
use App\Store;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $credentials = [
            [
                'sap_code' => 'CNL1090473636',
                'first_name' => 'Gabriel',
                'last_name' => 'Ruiz',
                'email' => 'gruiz@multicredito.com',
                'password' => Hash::make('123123')
            ],

            [
                'sap_code' => 'e1090473636',
                'first_name' => 'Empleado',
                'last_name' => 'Milan',
                'email' => 'empleado@milan.com',
                'password' => Hash::make('123123')
            ],
        ];

        foreach ($credentials as $user) {
            $find = preg_match("/^CNL/i", $user['sap_code']);
            if ($find == 1) {
                $user = User::create($user);
                $user->assignRole('store-admin');
            }

            $find = preg_match("/^E/i", $user['sap_code']);
            if ($find == 1) {
                $user = User::create($user);
                $user->assignRole('employee');
            }
        }

        $user = User::create([
            'sap_code' => '1090473636',
            'first_name' => 'Miller Gabriel',
            'last_name' => 'Contreras Ruiz',
            'email' => 'admin@multicredito.com',
            'password' => Hash::make('123123')
            ]);
            $user->assignRole('admin');
        }
    }
