<?php

use App\City;
use Illuminate\Database\Seeder;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cities = [
            [
                'name' => 'Santander',
                'department_id' => null
            ],
            [
                'name' => 'Bucaramanga',
                'department_id' => 1
            ],
            [
                'name' => 'Norte de Santander',
                'department_id' => null
            ],
            [
                'name' => 'Cúcuta',
                'department_id' => 3
            ]
        ];

        foreach ($cities as $city ) {
            City::create($city);
        }

        // $json_path = resource_path('json\colombia.min.json');
        // $json_file = file_get_contents($json_path);
        // $data = json_decode($json_file, true);
        
        
        // foreach ($data as $department) {
        //     $m_department = City::create([
        //         'name' => $department['departamento']
        //     ]);

        //     foreach ($department['ciudades'] as $city) {
        //         City::create([
        //             'name' => $city,
        //             'department_id' => $m_department['id']
        //         ]);
        //     }
        // }
    }
}
