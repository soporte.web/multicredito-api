<?php

namespace App\Http\Controllers;

use App\User;
use App\Store;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\RegisterStoreRequest;
use Illuminate\Support\Facades\DB;

class RegisterUserStoreController extends Controller
{
    /**METODO PARA REGISTRAR USUARIO CON TIENDA */
    public function register(RegisterStoreRequest $request){
        
        $user = $this->userSave(request()->all());
        if($user){
            $store = $this->storeSave(request()->all(),$user);
            if ($store) {
                return response()->json(['message' => 'Registro Creado con Exito!'], 201);
            }else{
                return response()->json(['message' => 'La Tienda ya existe!'], 400);
            }
        }else{
            return response()->json(['message' => 'El usuario ya existe!'], 400);
        }
    }


    public function storeSave($request, $user){
        $data = [
            'nit' => $request['nit'],
            'name' => $request['name'],
            'address' => $request['address'],
            'phone' => $request['phone'],
            'city_id' => $request['city_id'],
            'email' => $request['email'],
            'neighborhood' => $request['neighborhood'],
            'user_id' => $user->id
        ];
        /**Busco la tienda en la base de datos */
        $store = Store::withTrashed()->where('nit',$request['nit'])->first();

        /**Valido si existe una tienda con el nit */
        if ($store) {
            /**Valido si está en los registros eliminados */
            if($store->trashed()){
                /**Actualizo con los datos que entran desde la peticion*/
                $store = $this->updateStore($data, $store);
                /** Restablesco la tienda */
                $store->restore();
            }
        }else{
            /**Si no existe ninguna tienda con el nit, se crea */
            $store = Store::create($data);
        }
        return $store;
    }

    public function userSave($request){
        $data = [
            'sap_code' => $request['sap_code'],
            'first_name' => $request['first_name'],
            'last_name'=>$request['last_name'],
            'email' => $request['user_email'],
            'password' => Hash::make($request['password'])
        ]; 
        /**Busco la tienda en la base de datos */
        $user = User::withTrashed()->where('sap_code',$request['sap_code'])->first();
        /**Valido si existe una tienda con el nit */
        if ($user) {
            /**Valido si está en los eliminados */
            if($user->trashed()){
                /**Actualizo con los datos con los que entran */
                $user = $this->updateUser($data, $user);
                /** Restablesco la tienda */
                $user->restore();
            }
        }else{
            /**Si no existe ninguna tienda con el nit, se crea */
            $user = User::create($data);
        }

        return $user;
    }

    /**Metodo para actualizar la tienda */
    public function updateStore($data, $store)
    {
        $store->nit = $data['nit'];
        $store->name = $data['name'];
        $store->address = $data['address'];
        $store->neighborhood = $data['neighborhood'];
        $store->city = $data['city'];
        $store->phone = $data['phone'];
        $store->email = $data['email'];
        $store->user_id = $data['user_id'];

        $store->save();

        return $store;
    }

    /**Metodo para Actualizar el Usuario */
    public function updateUser($data, $user)
    {
        $user->sap_code = $data['sap_code'];
        $user->first_name = $data['first_name'];
        $user->last_name = $data['last_name'];
        $user->password = $data['password'];
        
        $user->save();

        return $user;
    }
}
