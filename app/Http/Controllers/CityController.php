<?php

namespace App\Http\Controllers;

use App\City;
use Illuminate\Http\Request;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(empty($_GET)){
            $cities = City::whereNull('department_id')
            ->with('childrenCities')->get();
        }else{
            if($_GET['departments']==true){
                $cities = City::whereNull('department_id')->get();
            }
        }
        return response()->json($cities, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $city = City::where('id',$id)->with('childrenCities')->get();
        return response()->json($city, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $city)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function destroy(City $city)
    {
        //
    }
}
