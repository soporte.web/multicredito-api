<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Http\Controllers\Controller;
use App\Transformers\UserTransformer;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api')->except(['login','register']);
        // $this->middleware('role_or_permission:admin|customer-get')->only('me');
    }

    /**
     * API Register
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(UserRequest $request)
    {
        $data = [
        'sap_code' => $request->sap_code,
        'first_name' => $request->first_name,
        'last_name' => $request->last_name,
        'email' => $request->email,
        'password' => Hash::make($request->password)
        ];
        
        $user = User::withTrashed()->where('sap_code',$request->sap_code)->first();
        if($user){
            if($user->trashed()){
                $user = $this->updateUser($data, $user);

                $user->restore();
                $user->assignRole('store-admin');
            }else{
                return response()->json(['message' => 'Ya existe un usuario ese Usuario'], 400);
            }
        }else{
            $user = User::create($data);
        }

        $user->assignRole('store-admin');

        return response()->json(['message' => 'Registro Exitoso!!'],201);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['sap_code', 'password']);

        if (!$token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        $user = (new UserTransformer)->transform(auth()->user());

        return response()->json(['user'=> $user],200);        
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Cierre de sesion exitoso!'],200);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 1440
        ]);
    }

    /**Metodo para Actualizar el Usuario */
    public function updateUser($data, $user)
    {
        $user->sap_code = $data['sap_code'];
        $user->first_name = $data['first_name'];
        $user->last_name = $data['last_name'];
        $user->password = $data['password'];
        
        $user->save();

        return $user;
    }

    
}
