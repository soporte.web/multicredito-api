<?php

namespace App\Http\Controllers;

use App\City;
use App\Department;
use App\Http\Controllers\Controller;


class FillCitiesController extends Controller
{
    public function fillCities(){
        $json_path = resource_path('json\colombia.min.json');
        $json_file = file_get_contents($json_path);
        $data = json_decode($json_file, true);
        
        
        foreach ($data as $department) {
            $m_department = Department::create([
                'name' => $department['departamento']
                ]
            );
            foreach ($department['ciudades'] as $city) {
                City::create([
                    'name' => $city,
                    'department_id' => $m_department->id
                ]);
            }
        }

    }
}
