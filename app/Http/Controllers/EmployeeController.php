<?php

namespace App\Http\Controllers;

use App\Store;
use App\Employee;
use App\Http\Requests\EmployeeRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Transformers\EmployeeTransformer;
use App\Transformers\StoreListTransformer;
use App\Transformers\EmployeeListTransformer;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class EmployeeController extends Controller
{
    use AuthenticatesUsers;

    public function __construct()
    {
        $this->middleware('auth:api,emp')->except('login');
        $this->middleware('role_or_permission:store-employee')->only('me');
        $this->middleware('role_or_permission:admin|employee-store-transfer')->only('employeeTransfer');
        $this->middleware('role_or_permission:admin|employee-store-create')->only('store');
        $this->middleware('role_or_permission:admin')->only('index');
        $this->middleware('role_or_permission:admin|store-employee-edit')->only('update');
        $this->middleware('role_or_permission:admin|store-employee-delete')->only('destroy');
        $this->middleware('role_or_permission:admin|store-admin')->only('show');
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::withTrashed()->get();

        $employees = fractal()->collection($employees)->transformWith(new EmployeeListTransformer)->toArray()['data'];
        return response()->json(['employees' => $employees], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeRequest $request)
    {
        $data = [
            'employee_id' => $request->employee_id,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'password' => Hash::make($request->password),
            'store_id' => $request->store_id
        ];
        $message = 'Usuario creado correctamente';

        $emp = Employee::withTrashed()->where('employee_id', $request->employee_id)->first();

        if ($emp) {
            if ($emp->trashed()) {

                $emp = $this->updateEmployee($request, $emp);

                $emp->restore();
            } else {
                return response()->json(['message' => 'El usuario ya existe!'], 400);
            }
        } else {
            $emp = Employee::create($data);
        }

        $emp->assignRole('store-employee');
        $emp = (new EmployeeTransformer)->transform($emp);

        return response()->json(['message' => $message, 'employee' => $emp], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employee = Employee::find($id);
        if (!$employee) {
            return response()->json(['message' => 'Registro No Encontrado'], 404);
        }
        $employee = (new EmployeeTransformer)->transform($employee);

        return response()->json(['employee' => $employee], 200);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeRequest $request, Employee $employee)
    {
        $employee = (new EmployeeTransformer)->transform($this->updateEmployee($request,$employee));

        return response()->json(['employee' => $employee], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        $employee->delete();
        return response()->json(['message'=>'Registro Eliminado'], 200);
    }

    public function login()
    {
        $credentials = request(['employee_id', 'password']);

        if (!$token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    public function me()
    {
        $me = auth()->user();
        $me = (new EmployeeTransformer)->transform($me);
        return response()->json(['user' => $me], 200);
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 1440
        ]);
    }

    public function employeeTransfer(Request $request)
    {
        $employee = Employee::find($request->employee_id);
        if (!$employee) {
            return response()->json(['message' => 'El Empleado NO se Encuentra'], 404);
        }

        $employee->store_id = $request->new_store_id;
        $employee->save();

        $old_store = (new StoreListTransformer)->transform(Store::find($request->old_store_id));
        $new_store = (new StoreListTransformer)->transform(Store::find($request->new_store_id));

        return response()->json(
            [
                'message' => 'Empleado Transferido Correctamente',
                'old' => $old_store,
                'new' => $new_store
            ],
            200
        );
    }

    public function updateEmployee($request, $employee)
    {
        $employee->employee_id = $request->employee_id;
        $employee->first_name = $request->first_name;
        $employee->last_name = $request->last_name;
        $employee->password = Hash::make($request->password);
        $employee->store_id = $request->store_id;

        $employee->save();
        
        return $employee;
    }
}
