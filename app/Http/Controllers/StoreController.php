<?php

namespace App\Http\Controllers;

use App\Store;
use Illuminate\Http\Request;
use App\Http\Requests\StoreRequest;
use App\Transformers\StoreListTransformer;
use App\Transformers\StoreTransformer;

class StoreController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->except(['store']);
        $this->middleware('role_or_permission:admin|store-admin|store-get-my')->only(['index']);
        $this->middleware('role_or_permission:admin|store-edit')->only(['update']);
        $this->middleware('role_or_permission:admin|store-get')->only(['show']);
        $this->middleware('role_or_permission:admin|store-delete')->only(['destroy']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        if($user->hasRole('admin')){
            $data = Store::all();
        }elseif ($user->hasPermissionTo('store-get-my')) {
            $data = $user->stores;
        }

        $data = (new StoreListTransformer)->transform($data);
        /**Obtengo todos las tiendas  */
        /** Retorno un objeto JSON con todas las tiendas */
        return response()->json(['message'=>$data], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $message = 'Tienda creada con Exito!';
        /**Busco la tienda en la base de datos */
        $store = Store::withTrashed()->where('nit',$request->nit)->first();
        /**Valido si existe una tienda con el nit */
        if ($store) {
            /**Valido si está en los eliminados */
            if($store->trashed()){
                /**Actualizo con los datos con los que entran */
                $store = $this->updateStore($request, $store);
                /** Restablesco la tienda */
                $store->restore();
            }else{
                /** Si no está en los eliminados retorno mensaje de error */
                return response()->json(['message' => 'Ya existe una tienda con ese Nit'], 400);
            }
        }else{
            /**Si no existe ninguna tienda con el nit, se crea */
            $store = Store::create($request->all());
        }
        /**Formateo el objeto  */
        $store = (new StoreTransformer)->transform($store);

        return response()->json(['message' => $message, 'store' => $store], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $store = Store::find($id);
        if(!$store){
            return response()->json(['message'=>'Registro no encontrado!'], 404);
        }
        $store = (new StoreTransformer)->transform($store);
        return response()->json(['store' => $store], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Store $store)
    {
        $message = 'Registro Actualizado correctamente!';
        $store = $this->updateStore($request, $store);

        return response()->json(['message' => $message,'store' => $store], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function destroy(Store $store)
    {
        $store->delete();
        return response()->json(['message' => 'El registro de tienda se ha eliminado!'], 200);
    }

    public function updateStore(Request $request, $store)
    {
        $store->nit = $request->nit;
        $store->name = $request->name;
        $store->address = $request->address;
        $store->neighborhood = $request->neighborhood;
        $store->city_id = $request->city_id;
        $store->phone = $request->phone;
        $store->email = $request->email;
        $store->user_id = $request->user_id;

        $store->save();

        return $store;
    }
}
