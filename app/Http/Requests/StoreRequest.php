<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\JsonResponse;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nit' => 'required|unique:stores,nit,NULL,id,deleted_at,NULL|min:5|max:50',
            'name' => 'required|unique:stores,name,NULL,id,deleted_at,NULL|min:3|max:50',
            'address' => 'required|min:10|max:50',
            'neighborhood' => 'required|min:4|max:30',
            'city_id' => 'required|numeric',
            'phone' => 'required|numeric|digits_between:5,10',
            'email' => 'required|email|min:10|max:50'
        ];
    }

    public function messages(){
        return[
            'unique' => 'El :attribute ya está en uso',
            'required' => 'El campo :attribute es requerido',
            'min' => 'El campo :attribute es muy corto',
            'max' => 'El campo :attribute es muy Largo',
            'numeric' => 'El campo :attribute debe ser numérico',
            'digits_between'=> 'El :attribute debe estar entre 5 y 10 caracteres',
        ]; 
    }

    public function attributes()
    {
        return [
            'name' => 'Nombre de la Tienda',
            'address' => 'Direccion',
            'neighborhood' => 'Barrio',
            'city_id' => 'Ciudad',
            'phone' => 'Teléfono',
            'email' => 'Corre Electrónico'
        ];
    }

    public function response(array $errors){
        if($this->expectsJson()){
            return new JsonResponse($errors, 422);
        }
    }
}
