<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterStoreRequest extends FormRequest
{
  public $formRequests = [
    UserRequest::class,
    StoreRequest::class
  ];
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    $formRequests = [
      UserRequest::class,
      StoreRequest::class
    ];
    $rules = [
      'user_email' => 'required|unique:users,email,NULL,id,deleted_at,NULL|min:10,max:50'
    ];

    foreach ($formRequests as $source) {
      $rules = array_merge(
        $rules,
        (new $source)->rules()
      );
    }
    return $rules;
  }

  public function messages()
  {
    $formRequests = [
      UserRequest::class,
      StoreRequest::class
    ];
    $messages = [];

    foreach ($formRequests as $source) {
      $messages = array_merge(
        $messages,
        (new $source)->messages()
      );
    }
    return $messages;
  }

  public function attributes()
  {
    $formRequests = [
      UserRequest::class,
      StoreRequest::class
    ];
    $attributes = [
      'user_email' => 'Correo Electrónico'
    ];

    foreach ($formRequests as $source) {
      $attributes = array_merge(
        $attributes,
        (new $source)->attributes()
      );
    }
    return $attributes;
  }
}
