<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EmployeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'employee_id' => 'required|min:6|max:20|unique:employees,deleted_at,NULL',
            'first_name' => 'required|min:3|max:30',
            'last_name' => 'required|min:3|max:30',
            'password' => 'required|digits_between:6,20',
        ];
    }

    public function messages(){
        return [
            'required'=>'El campo :attribute es requerido',
            'min' => 'El campo :attribute es muy corto',
            'max' => 'El campo :attribute es muy largo',
            'unique' => 'El campo :attribute ya está en uso',
            'digits_between' => 'La :attribute debe tener entre 6 y 20 caracteres'
        ];
    }

    public function attributes(){
        return [
            'employee_id' => ' Numero de Cedula',
            'first_name' => 'Nombres',
            'last_name' => 'Apellidos',
            'password' => 'Contraseña'
        ];

    }
}
