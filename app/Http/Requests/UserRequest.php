<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sap_code' => 'required|unique:users,sap_code,NULL,id,deleted_at,NULL|max:50',
            'last_name' => 'required|max:100',
            'first_name' => 'required|max:255',
            'password' => 'required|min:6|max:50'
        ];
    }

    public function messages(){
        return [
            'required' => ':attribute es requerido',
            'max' => 'El campo :attribute es muy largo',
            'min' => 'El campo :attribute es muy corto',
            'unique' => 'El :attribute ya está en uso',
        ];
    }

    public function attributes(){
        return [
            'sap_code' => 'Codigo SAP',
            'first_name' => 'Nombres',
            'last_name' => 'Apellidos',
            'password' => 'Contraseña'
        ];
    }
}
