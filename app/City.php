<?php

namespace App;

use App\Transformers\CityTransformer;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $fillable = ['name','department_id'];
    protected $hidden = ['created_at','updated_at'];
    
    public function department()
    {
        return $this->hasMany('App\City', 'department_id', 'id');
    }

    public function childrenCities()
    {
        return $this->hasMany('App\City','department_id', 'id');
    }

    public function stores()
    {
        return $this->hasMany('App\Store', 'city_id', 'id');
    }
}
