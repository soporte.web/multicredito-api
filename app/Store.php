<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Store extends Model
{
    use SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nit', 'name', 'address', 'phone', 'email', 'neighborhood', 'city_id', 'user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function city()
    {
        return $this->belongsTo('App\City', 'city_id', 'id');
    }
    
    public function employees()
    {
        return $this->hasMany('App\Employee', 'store_id', 'id');
    }

    /**
     * ACCESSORS & MUTATORS
     */
    /**
     * NIT
     * @param $value
     * @return string
     */
    public function getNitAttribute($value){
        return ucwords($value);
    }
    public function setNitAttribute($value){
        $this->attributes['nit'] = strtolower($value);
    }

    /**
     * NAME
     * @param $value
     * @return string
     */
    public function getNameAttribute($value){
        return ucwords($value);
    }
    public function setNameAttribute($value){
        $this->attributes['name'] = strtolower($value);
    }

    /**
     * ADDRESS
     * @param $value
     * @return string
     */
    public function getAddressAttribute($value){
        return ucwords($value);
    }
    public function setAddressAttribute($value){
        $this->attributes['address'] = strtolower($value);
    }

    /**
     * EMAIL
     * @param $value
     */
    public function setEmailAttribute($value){
        $this->attributes['email'] = strtolower($value);
    }

}
