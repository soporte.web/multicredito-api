<?php

namespace App;

use App\Transformers\UserTransformer;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use App\Transformers\RoleListTransformer;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable, HasRoles, SoftDeletes;
    
    protected $guard_name = 'api';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sap_code','first_name','last_name','email', 'password',
    ];

    public function stores()
    {
        return $this->hasMany('App\Store');
    }

    // public function creditRequest()
    // {
    //     return $this->hasMany('App\CreditRequest', 'user_id', 'id');
    // }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    // protected $casts = [
    //     'email_verified_at' => 'datetime',
    // ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [
            'id' => $this->id,
            'user_id' => $this->sap_code,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'permissions' => (new RoleListTransformer)->transform($this)
        ];
    }


    /** 
     * ACCESSORS & MUTATORS
     */
    /** CODIGO SAP */
    /**
     * @param $value
     * @return string
     */
    public function getSapCodeAttribute($value){
        return strtoupper($value);
    }
    /**
     * @param $value
     * @return string
     */
    public function setSapCodeAttribute($value){
        $this->attributes['sap_code'] = strtolower($value);
    }

    /**
     * NOMBRES
     * @param $value
     * @return string
     */
    public function getFirstNameAttribute($value){
        return ucwords($value);
    }
    /**
     * @param $value
     * @return string
     */
    public function setFirstNameAttribute($value){
        $this->attributes['first_name'] = strtolower($value);
    }

    /**
     * APELLIDOS
     * @param $value
     * @return string
     */
    public function getLastNameAttribute($value){
        return ucwords($value);
    }
    /**
     * @param $value
     * @return string
     */
    public function setLastNameAttribute($value){
        $this->attributes['last_name'] = strtolower($value);
    }

    /**
     *  EMAIL
     * @param $value
     */
    public function setEmailAttribute($value){
        $this->attributes['email'] = strtolower($value);
    }
}
