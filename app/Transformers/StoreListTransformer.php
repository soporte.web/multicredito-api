<?php

namespace App\Transformers;

use App\Store;
use League\Fractal\TransformerAbstract;

class StoreListTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Store $store)
    {
        return [
            'id'=>$store->id,
            'name'=>$store->name,
            'city'=>$store->city->name
        ];
    }
}
