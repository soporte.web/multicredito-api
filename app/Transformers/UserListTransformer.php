<?php

namespace App\Transformers;

use App\User;
use League\Fractal\TransformerAbstract;

class UserListTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(User $user)
    {
        return [
            'id'=>(int)$user->id,
            'sap_code'=>(string)$user->sap_code,
            'first_name'=>(string)$user->first_name,
            'last_name'=>(string)$user->last_name,
            'email'=>(string)$user->email,
            'stores'=>(new StoreListTransformer)->transform($user->stores)
        ];
    }
}
