<?php

namespace App\Transformers;

use App\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(User $user)
    {
        return [
            'id'=>(int)$user->id,
            'sap_code'=>(string)$user->sap_code,
            'first_name'=>(string)$user->first_name,
            'last_name'=>(string)$user->last_name,
            'email'=>(string)$user->email,
            'stores'=>fractal()->collection($user->stores)->transformWith(new StoreListTransformer)->toArray()['data'],
            'permissions' => (new RoleListTransformer)->transform($user)

        ];
    }
}
