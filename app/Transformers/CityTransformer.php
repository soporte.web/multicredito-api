<?php

namespace App\Transformers;

use App\City;
use League\Fractal\TransformerAbstract;

class CityTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(City $city)
    {   
        $department = City::find($city->department_id);
        return [
            'id' => (int)$city->id,
            'name'=>(string)$city->name,
            "department"=>(string)$department->name
        ];
    }
}
