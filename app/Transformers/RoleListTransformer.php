<?php

namespace App\Transformers;

use App\User;
use Illuminate\Database\Eloquent\Model;
use League\Fractal\TransformerAbstract;

class RoleListTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Model $user)
    {
        $permissions = $user->getAllPermissions();
        $name_permissions = [];/**Guarda los nombres de los permisos */
        foreach ($permissions as $permission ) {
            $permission_name = ['name' => $permission->name];
            array_push($name_permissions,(object)$permission_name);
        }
        return $name_permissions
        ;
    }
}
