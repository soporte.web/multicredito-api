<?php

namespace App\Transformers;

use App\Store;
use League\Fractal\TransformerAbstract;

class StoreTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Store $store)
    {
        return [
            'id'=>$store->id,
            'nit'=>$store->nit,
            'name'=>$store->name,
            'address'=>$store->address,
            'neighborhood'=>$store->neighborhood,
            'city'=>(new CityTransformer)->transform($store->city),
            'employees'=>fractal()->collection($store->employees)->transformWith(new EmployeeListTransformer)->toArray()['data']
        ];
    }
}
