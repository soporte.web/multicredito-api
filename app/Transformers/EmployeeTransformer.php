<?php

namespace App\Transformers;

use App\Employee;
use League\Fractal\TransformerAbstract;

class EmployeeTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Employee $employee)
    {
        return [
            'id'=>(int)$employee->id,
            'employee_id' => (string)$employee->employee_id,
            'first_name'=> (string)$employee->first_name,
            'last_name'=> (string)$employee->last_name,
            'store'=>(new StoreListTransformer)->transform($employee->store)
        ];
    }
}
