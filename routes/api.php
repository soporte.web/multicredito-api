<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth/user',
], function ($router) {
    Route::post('register', 'AuthController@register');
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
    Route::post('register-store','RegisterUserStoreController@register');
});

Route::group([
    'middleware' => 'assign.guard:emp',
    'prefix' => 'auth/employee'
], function ($router) {
    Route::post('login', 'EmployeeController@login');
    Route::post('me','EmployeeController@me');
});

Route::resource('stores', 'StoreController');
Route::resource('cities', 'CityController');
Route::resource('employees', 'EmployeeController');
Route::post('employees/transfer','EmployeeController@employeeTransfer');
